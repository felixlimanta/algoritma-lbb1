# Author: Felix Limanta (felixlimanta@gmail.com)

source("R/plot.R")

crstab <- function() {
    df <- InitData()
    # PlotAlcoholConsumptionFrequency(df)
    # HeatmapAlcoholConsumptionGradeAvg(df)
    # PlotGradeDensity(df)
    # BoxplotAlcoholConsumptionGrade(df)
    SwarmPlotAlcoholConsumptionGrade(df)
}